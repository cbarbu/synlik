#========================
# GPL
# Corentin M. Barbu
# Testing of the convergence diagnostics
#========================
library("testthat")
test_that("CombinedDiag converged good",{
	set.seed(1000)
	nIt<-1000
	nMonitored<-10
	# should be converged
	fakeSamples<-matrix(rnorm(nIt*nMonitored),ncol=nMonitored)
	diag<-CombinedDiag(fakeSamples,logFile=NULL)
	expect_true(diag$ok==1)
})
test_that("CombinedDiag not-converged good",{
	set.seed(1000)
	nIt<-1000
	nMonitored<-10
	# get not converged sample
	fakeSamples<-matrix(rnorm(nIt*nMonitored),ncol=nMonitored)
	fakeSamples[,1]<-fakeSamples[,1]+(1:nIt)/nIt

	# should not be converged
	diag<-CombinedDiag(fakeSamples,logFile=NULL)
	expect_true(diag$ok==0)
})

test_that("GelmanRubinWrapper ok",{
	  set.seed(1000)
	  # fake chains
	  chain1 <- cbind(rnorm(10000),rlnorm(10000,10,1))
	  chain2 <- cbind(rnorm(10000),rlnorm(10000,10,1))

	  chain1bis <- as.data.frame(chain1)
	  chain2bis <- as.data.frame(chain2)

	  # prepare for the analysis
	  mins <- c(-Inf,0);
	  maxs <- c(+Inf,+Inf);
	  chains<-list(chain1,chain2)
	  chainsBis<-list(chain1,chain2)
	  # analysis
	  gr<-GelmanRubinWrapper(chains,mins,maxs)
	  expect_true(gr$ok)
	  grBis<-GelmanRubinWrapper(chainsBis,mins,maxs)
	  expect_identical(grBis,gr)
})




