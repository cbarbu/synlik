#=========================
# Reweight an MCMC sample potentially screwed by resampling 
# of accepted values
# the parameters and lik should have all the values intempted
# (accepted and rejected) and not only the sampled values
#=========================

#' @title Get posterior shape from serie of likelihoods
#' @description As the sampling we use for synthetic likelihood does resample accepted values, the posterior distribution of the chain is not guarantedd (Hortig2011). In this function any vectors of parameter value and lik can be passed to get a likelihood based distribution.
#' @param param values of a parameter 
#' @param lik values corresponding to the parameter
#' @param nsmooth number of points per smoothing unit
#' @return a list with items param, lik, and normfac - param and lik corresponding to the ordered by parameter value, smoothed and shorter parameter values and likelihoods - normfac the normalization value for the function
#' @examples 
#' param <- rnorm(1000001) 
#' lik <- dnorm(param)
#' out<-reweight.marginal.distributions(param,lik)
#' plot(out$param,out$lik,pch=".")
#' @export reweight.marginal.distributions

reweight.marginal.distributions<-function(param,lik,nsmooth=10){
  # remove NA
  isOK <- which(!is.na(param) & !is.na(lik))
  param <- param[isOK]
  lik <- lik[isOK]
  # reorder
  Oparam<-order(param)
  param<-param[Oparam]
  lik<-lik[Oparam]
  # smooth x
  Nparam<- nsmooth*(length(param)%/%nsmooth) # limit to multiple of nsmooth
  Mparam<-matrix(param[1:Nparam],nrow=nsmooth,byrow=FALSE) # put in matrix
  Sparam<- apply(Mparam,2,sum)/nsmooth # avg on nsmooth points
  # smooth lik
  MLLH<-matrix(lik[1:Nparam],nrow=nsmooth,byrow=FALSE)
  SLLH<- apply(MLLH,2,sum)/nsmooth
  # then need to normalize (trapez method)
  diffs<-Sparam[2:length(Sparam)]- Sparam[1:(length(Sparam)-1)]
  int<-sum((SLLH[1:(length(Sparam)-1)]+SLLH[2:length(Sparam)])*diffs/2)
  SLLH<-SLLH/int

  return(list(param=Sparam,lik=SLLH,normfac=int))
}

#' @title Preanalysis for precision and coverage
#'
#' @description Given a model and a parameter set from/around which to simulate
#'    Determine precision and coverage expected from the synthetic likelihood  
#'    This is similar to cross-validation in the ABC world.
#' @param slObj See slObj objects(a list containing simulator, summary statistics, params, etc.) 
#' @param nRep the number of iterations per parameter point tested (defaults to 200)
#' @param simulator Overide the simulator in slObj
#' @param summaries Overide the corresponding value in slObj
#' @param statsOpt Overide the corresponding value in slObj
#' @param simulOpt Overide the corresponding value in slObj
#' @param params Overide the corresponding value in slObj
#' @param sampleParameters a table with columns of parameters and rows of different samples 
#' @param probs a vector of credible interval probability sizes (defaults to c(0.90, 0.95, 0.99))
#' @param ... arguments passed to oneDimPrecI
#' @return a list with a data frame with in lines the 
#'       different probabilities and 
#'       in columns the expected coverages according to each fitting 
#'       multivariate distribution. Returns NULL in case of error.
#'
#' @details  Need to be added
#' @author Corentin M. Barbu, Karthik Sethuraman
#' @seealso lik.cov.match, QuickCorrelationFromStats
#' @examples 
#' # to be added
#' @export preAnalysis 

preAnalysis <- function(slObj, nRep=50,simulator=NULL,summaries=NULL,statsOpt=NULL,simulOpt=NULL,params=NULL, sampleParameters, probs=c(0.90, 0.95, 0.99), ...){

  #check if enough repetitions  
  if(nRep < 50){
    warning("nRep < 50, setting nRep to 50")
    nRep <- 50
  }

  # get necessary items from slObj or optional arguments 
  if(is.null(simulator)){
    simulator<- slObj$simulator
  }
  if(is.null(summaries)){
    summaries<- slObj$summaries
  }
  if(is.null(statsOpt)){
    statsOpt<- slObj$statsOpt
  }
  if(is.null(simulOpt)){
    simulOpt<- slObj$simulOpt
  }
  if(is.null(params)){
    params<- slObj$params
  }
  if(is.null(dim(params))){ # force params format to 2 dimensions
    params<-matrix(params,nrow=1)
  }
  if(length(summaries)==1){ # force list format for summaries
    summaries <- list(summaries)
    statsOpt<- list(statsOpt)
  }else{ # check summaries and statsOpt have same length)
    if(length(statsOpt) != length(summaries)){
      stop("verify statsOpt and summaries are their lengths are not compatible")
    }
  }

  # coerce sampleParameters into matrix (if data.frame)
  sampleParameters <- as.matrix(sampleParameters)

  # identify the number of stats generated per statfn
  data <- simulator(params[1,],simulOpt)
  nStats<- rep(0,length(summaries))
  for(i_statfn in 1:length(summaries)){
    statfn<- summaries[[i_statfn]]
    nStats[i_statfn] <- length(statfn(data,statsOpt[[i_statfn]]))
  }

  # list to hold stats over true simulations
  trueStatsOut <- list()
  for(i_statfn in 1:length(summaries)){ # loop over stat functions
    trueStatsOut[[i_statfn]]<-as.matrix(array(0, dim=c(nRep, nStats[i_statfn])))
  }

  # generate simulations and summary statistics over true params (in params)
  param <- params[1, ]
  for(i_simul in 1:nRep){ #generate a simulation
    simOut <- simulator(param, simulOpt)
    for(i_statfn in 1:length(summaries)){ #compute summary statistics of simulation
      statfn <- summaries[[i_statfn]]
      trueStatsOut[[i_statfn]][i_simul,] <- statfn(simOut, statsOpt[[i_statfn]])
    }
  }

  # list to hold stats over all simulations
  lStatsOut <- list()
  for(i_statfn in 1:length(summaries)){ # loop over stat functions
    lStatsOut[[i_statfn]]<-as.matrix(array(0, dim=c(nRep, nStats[i_statfn])))
  }

  # matrix to hold sample likelihoods P(trueStatsOut | lStatsOut)
  lSampleLLs <- list()
  for(i_statfn in 1:length(summaries)){
    lSampleLLs[[i_statfn]] <- mat.or.vec(dim(sampleParameters)[1], nRep)
  }

  # generate simulations and summary statistics over all the sample parameters
  nSetParams <- dim(sampleParameters)[1]
  for(i_param_set in 1:nSetParams){ #for each set of parameters
    param <- sampleParameters[i_param_set,]
    for(i_simul in 1:nRep){ #generate a simulation
      simOut <- simulator(param, simulOpt)
      for(i_statfn in 1:length(summaries)){ #compute summary statistics of simulation
        statfn <- summaries[[i_statfn]]
        lStatsOut[[i_statfn]][i_simul,] <- statfn(simOut, statsOpt[[i_statfn]])
      }
    }

    for(i_statfn in 1:length(summaries)){ #calculate ll for param
      lSampleLLs[[i_statfn]][i_param_set, ] <- synLik(t(lStatsOut[[i_statfn]]), t(trueStatsOut[[i_statfn]]))
    }

  }

  # matrix (or vec) to hold true likelihoods P(trueStatsOut | trueStatsOut)
  trueLLs <- mat.or.vec(nRep, length(summaries))
  param <- params[1, ]
  for(i_simul in 1:nRep){ #generate a simulation
    simOut <- simulator(param, simulOpt)
    for(i_statfn in 1:length(summaries)){ #compute LL of the simulation 
      statfn <- summaries[[i_statfn]]
      trueStats <- statfn(simOut, statsOpt[[i_statfn]])
      trueLLs[i_simul, i_statfn] <- synLik(t(trueStatsOut[[i_statfn]]), trueStats)
    }
  }

  # calculate precision for summaries
  ## note:
  ## |1, 3|
  ## |2, 4|
  ## becomes 1, 2, 3, 4 as vector

  precIntervals <- list() #stores precision interval information per summary statistic
  coverage <- list() #store coverage information per summary statistic

  for(i_statfn in 1:length(summaries)){ #for every summary statistic under consideration

    # make new device for plotting
    dev.new()
    dim <- as.integer(ceiling(sqrt(2*dim(sampleParameters)[2])))
    par(mfrow = c(dim, dim))

    lls <- exp(as.vector(lSampleLLs[[i_statfn]])) #vector of likelihoods (convert from log likelihoods)

    precIntervals[[i_statfn]] <- list() 
    coverage[[i_statfn]] <- list()  

    for(i_param in 1:dim(sampleParameters)[2]){
      thetas <- rep(sampleParameters[, i_param], nRep) #the marginal parameters

      # find the precision
      precOut <- oneDimPrecI(thetas, lls, prI = probs, plot = TRUE, ...)
      precIntervals[[i_statfn]][[i_param]] <- precOut

      # find the coverage
      normTrueLLs <- exp(trueLLs[, i_statfn]) / precOut$norm_fac
      pIn <- rep(0, length(probs)) #stores coverage for each given threshold
      names(pIn) <- probs
      for(i_pVal in 1:length(probs)){
        tr_ll <- precOut$cutoff_ll[i_pVal]
        pIn[i_pVal] <- length(which(normTrueLLs > tr_ll))/length(normTrueLLs)
      }

      coverage[[i_statfn]][[i_param]] <- pIn

      # plot the wanted coverage (probs) versus actual coverage (pIn)
      plot(probs, pIn, main = names(sampleParameters)[i_param], xlim = c(0, 1), ylim = c(0, 1))
      abline(a=0, b=1)
    }

    names(precIntervals[[i_statfn]]) <- names(sampleParameters)
    names(coverage[[i_statfn]]) <- names(sampleParameters)
  }

  names(precIntervals) <- summaries
  names(coverage) <- summaries  

  # return some understandable form of output (NOT TESTED)
  return(list(precIntervals = precIntervals, coverage = coverage))  
}

#' @title Generates likelihoods for a given set of parameters
#'
#' @description Given a model and a parameter set from/around which to simulate
#'		obtain likelihoods for all parameters
#' @param slObj See slObj objects(a list containing simulator, summary statistics, params, etc.) 
#' @param nRep the number of iterations per parameter point tested (defaults to 50)
#' @param simulator Overide the simulator in slObj
#' @param summaries Overide the corresponding value in slObj
#' @param statsOpt Overide the corresponding value in slObj
#' @param simulOpt Overide the corresponding value in slObj
#' @param params Overide the corresponding value in slObj
#' @param sampleParameters a table with columns of parameters and rows of different samples 
#' @return a list of 
#'	sampleParameters, same as passed object, 
#'	sampleLLs, a list of length number of summary statistics containing 
#'	data.frames of likelihoods generated from the sample parameters 
#'	with rows the number of sample parameters and columns nRep, 
#'	trueLLs, a dataframe of likelihoods generated from the true parameter 
#'	with rows nRep and columns different summaries.
#'
#' @details  Need to be added
#' @author Corentin M. Barbu, Karthik Sethuraman
#' @seealso preAnalysis
#' @examples 
#' # to be added
#' @export preSampleGenerator 

preSampleGenerator <- function(slObj, nRep=50, simulator=NULL,summaries=NULL,statsOpt=NULL,simulOpt=NULL,params=NULL, sampleParameters){

  #check if enough repetitions  
  if(nRep < 50){
    warning("nRep < 50, setting nRep to 50")
    nRep <- 50
  }

  # get necessary items from slObj or optional arguments 
  if(is.null(simulator)){
    simulator<- slObj$simulator
  }
  if(is.null(summaries)){
    summaries<- slObj$summaries
  }
  if(is.null(statsOpt)){
    statsOpt<- slObj$statsOpt
  }
  if(is.null(simulOpt)){
    simulOpt<- slObj$simulOpt
  }
  if(is.null(params)){
    params<- slObj$params
  }
  if(is.null(dim(params))){ # force params format to 2 dimensions
    params<-matrix(params,nrow=1)
  }
  if(length(summaries)==1){ # force list format for summaries
    summaries <- list(summaries)
    statsOpt<- list(statsOpt)
  }else{ # check summaries and statsOpt have same length)
    if(length(statsOpt) != length(summaries)){
      stop("verify statsOpt and summaries are their lengths are not compatible")
    }
  }

  # coerce sampleParameters into matrix (if data.frame)
  sampleParameters <- as.matrix(sampleParameters)

  # identify the number of stats generated per statfn
  data <- simulator(params[1,],simulOpt)
  nStats<- rep(0,length(summaries))
  for(i_statfn in 1:length(summaries)){
    statfn<- summaries[[i_statfn]]
    nStats[i_statfn] <- length(statfn(data,statsOpt[[i_statfn]]))
  }

  # list to hold stats over true simulations
  trueStatsOut <- list()
  for(i_statfn in 1:length(summaries)){ # loop over stat functions
    trueStatsOut[[i_statfn]]<-as.matrix(array(0, dim=c(nRep, nStats[i_statfn])))
  }

  # generate simulations and summary statistics over true params (in params)
  param <- params[1, ]
  for(i_simul in 1:nRep){ #generate a simulation
    simOut <- simulator(param, simulOpt)
    for(i_statfn in 1:length(summaries)){ #compute summary statistics of simulation
      statfn <- summaries[[i_statfn]]
      trueStatsOut[[i_statfn]][i_simul,] <- statfn(simOut, statsOpt[[i_statfn]])
    }
  }

  # list to hold stats over all simulations
  lStatsOut <- list()
  for(i_statfn in 1:length(summaries)){ # loop over stat functions
    lStatsOut[[i_statfn]]<-as.matrix(array(0, dim=c(nRep, nStats[i_statfn])))
  }

  # matrix to hold sample likelihoods P(trueStatsOut | lStatsOut)
  lSampleLLs <- list()
  for(i_statfn in 1:length(summaries)){
    lSampleLLs[[i_statfn]] <- mat.or.vec(dim(sampleParameters)[1], nRep)
  }

  # generate simulations and summary statistics over all the sample parameters
  nSetParams <- dim(sampleParameters)[1]
  for(i_param_set in 1:nSetParams){ #for each set of parameters
    param <- sampleParameters[i_param_set,]
    for(i_simul in 1:nRep){ #generate a simulation
      simOut <- simulator(param, simulOpt)
      for(i_statfn in 1:length(summaries)){ #compute summary statistics of simulation
        statfn <- summaries[[i_statfn]]
        lStatsOut[[i_statfn]][i_simul,] <- statfn(simOut, statsOpt[[i_statfn]])
      }
    }

    for(i_statfn in 1:length(summaries)){ #calculate ll for param
      lSampleLLs[[i_statfn]][i_param_set, ] <- synLik(t(lStatsOut[[i_statfn]]), t(trueStatsOut[[i_statfn]]))
    }

  }

  # matrix (or vec) to hold true likelihoods P(trueStatsOut | trueStatsOut)
  trueLLs <- mat.or.vec(nRep, length(summaries))
  param <- params[1, ]
  for(i_simul in 1:nRep){ #generate a simulation
    simOut <- simulator(param, simulOpt)
    for(i_statfn in 1:length(summaries)){ #compute LL of the simulation 
      statfn <- summaries[[i_statfn]]
      trueStats <- statfn(simOut, statsOpt[[i_statfn]])
      trueLLs[i_simul, i_statfn] <- synLik(t(trueStatsOut[[i_statfn]]), trueStats)
    }
  }

  return(list(sampleParameters=sampleParameters, sampleLLs=lSampleLLs, trueLLs=trueLLs))
}

#' @title Coverage and precision analysis
#' @description Given sample parameters, sample lls, and true lls
#'	generate a precision interval and assess the coverage of the true value
#' @param params true parameters
#' @param sampleParameters a data.frame with rows of different samples and columns of parameters
#' @param trueLLs a data.frame of true likelihoods (see preSampleGenerator)
#' @param sampleLLs a list of sample likelihoods (see preSampleGenerator) 
#' @param probs a vector of credible interval probability sizes (defaults to c(0.90, 0.95, 0.99))
#' @param ... arguments passed to oneDimPrecI
#' @return a list of precIntervals, a list of oneDimPrecI objects, 
#'	and coverage, a list of the same length as sampleLLs with vectors giving coverage
#'	with vectors giving coverage at each prob
#'
#' @details  Need to be added
#' @author Corentin M. Barbu, Karthik Sethuraman
#' @seealso preAnalysis
#' @examples 
#' # to be added
#' @export PrecCovAnalysis 

## TODO: NOT READY TO USE!
PrecCovAnalysis <- function(params, sampleParameters, trueLLs, sampleLLs, probs=c(0.90, 0.95, 0.99), ...){

  if(is.null(dim(params))){ # force params format to 2 dimensions
    params<-matrix(params,nrow=1)
  }

  # calculate precision for summaries
  ## note:
  ## |1, 3|
  ## |2, 4|
  ## becomes 1, 2, 3, 4 as vector

  precIntervals <- list() #stores precision interval information per summary statistic
  coverage <- list() #store coverage information per summary statistic

  for(i_statfn in 1:length(sampleLLs)){ #for every summary statistic under consideration

    # make new device for plotting
    dev.new()
    dim <- as.integer(ceiling(sqrt(2*dim(sampleParameters)[2])))
    par(mfrow = c(dim, dim))

    lls <- exp(as.vector(lSampleLLs[[i_statfn]])) #vector of likelihoods (convert from log likelihoods)

    precIntervals[[i_statfn]] <- list() 
    coverage[[i_statfn]] <- list()  

    for(i_param in 1:dim(sampleParameters)[2]){
      thetas <- rep(sampleParameters[, i_param], nRep) #the marginal parameters

      # find the precision
      precOut <- oneDimPrecI(thetas, lls, prI = probs, plot = TRUE, ...)
      precIntervals[[i_statfn]][[i_param]] <- precOut

      # find the coverage
      normTrueLLs <- exp(trueLLs[, i_statfn]) / precOut$norm_fac
      pIn <- rep(0, length(probs)) #stores coverage for each given threshold
      names(pIn) <- probs
      for(i_pVal in 1:length(probs)){
        tr_ll <- precOut$cutoff_ll[i_pVal]
        pIn[i_pVal] <- length(which(normTrueLLs > tr_ll))/length(normTrueLLs)
      }

      coverage[[i_statfn]][[i_param]] <- pIn

      # plot the wanted coverage (probs) versus actual coverage (pIn)
      plot(probs, pIn, main = names(sampleParameters)[i_param], xlim = c(0, 1), ylim = c(0, 1))
      abline(a=0, b=1)
    }

    names(precIntervals[[i_statfn]]) <- names(sampleParameters)
    names(coverage[[i_statfn]]) <- names(sampleParameters)
  }

  names(precIntervals) <- names(sampleLLs)
  names(coverage) <- names(sampleLLs) 

  return(list(precIntervals = precIntervals, coverage = coverage))  
}


#' @title Form a normalized one-dimensional precision interval 
#'
#' @description Given a set of thetas (x) and corresponding likelihoods (y),
#'    form one dimentional normalized density function and compute credible regions
#'    (High Posterior Density Sets) for all alphas given in prI
#' @param x parameter values
#' @param y density at parameter values
#' @param steps number of steps to bin the area, defaults to length(x)/2
#' @param prI the desired coverage for the region (1-alpha)
#' @param plot whether or not to plot normalized posterior, defaults to FALSE
#' @param plotPoints whether or not to plot original data on posterior, defaults to FALSE
#' @param plotThresholds whether or not to plot horizontal likelihood cutoffs, defaults to FALSE
#' @param xlim xlim of plot (if not passed, no xlim)
#' @param ylim ylim of plot (if not passed, no ylim)
#' @param xlab x axis label
#' @param ylab y axis label
#' @param col color of the curve in the plot
#' @param ... passed to smooth.spline function
#' @return a list with:\itemize{
#'         \item{cutoff_ll}{ the cutoffs for each of the prI,} 
#'         \item{crI}{ the credible intervals according to the smoothing, one line per value in cutoff_ll: range(px[py>cutoff_ll])}
#'         \item{px}{ the normalized x}
#'         \item{py}{ the normalized y}
#'         \item{x and y}{ the original x and y inputs}
#'         \item{norm_fac}{ the normalization constant}
#'         }
#' @details #to be added
#' @author Corentin M. Barbu, Karthik Sethuraman
#' @seealso pre.analysis
#' @examples 
#'
#' # use a uniform sample with normal density function
#' thetas <- runif(1000, -5, 5)
#' dens <- dnorm(thetas)
#' oneDprec <- oneDimPrecI(thetas, dens, plot = TRUE)
#'
#' # make the sample more sparse
#' thetas_sparse <- runif(100, -5, 5)
#' dens_sparse <- dnorm(thetas_sparse)
#' oneDprec_sparse <- oneDimPrecI(thetas_sparse, dens_sparse, plot = TRUE)
#'
#' @export oneDimPrecI

oneDimPrecI <- function(x, y, steps=length(x)/2, prI=c(0.5, 0.75, 0.95), 
                        plot=FALSE, plotPoints = FALSE, plotThresholds = FALSE, 
                        xlim, ylim,
                        xlab="Parameter value",ylab="Density", col="grey",initLogs=FALSE,setNA=0, ...){
	y[is.na(y)]<-setNA

	# TODO(corentin): comment that a bit more inline
	pred_smooth <- smooth.spline(x, y, ...)
  px <- seq(min(x), max(x), length.out=steps+1)
  if(initLogs){
	  py <- exp(predict(pred_smooth, px)$y)
  }else{
	  py <- predict(pred_smooth, px)$y
  }


  py[which(py < 0)] <- 0 #set things with negative density to zero density  

  xpairs <- matrix(c(px[1:(length(px)-1)], px[2:length(px)]), ncol = 2)
  ypairs <- matrix(c(py[1:(length(py)-1)], py[2:length(py)]), ncol = 2)

  trap_widths <- apply(xpairs, 1, function(xpair){return(abs(xpair[2]-xpair[1]))})
  trap_heights <- apply(ypairs, 1, mean)
  trap_areas <- trap_widths * trap_heights

  total_area <- sum(trap_areas) #total area, normalization factor
  py <- py / total_area #normalize py
  trap_heights <- trap_heights / total_area #normalize trap_heights
  ypairs <- ypairs/ total_area # normalize the py pair values
  trap_areas <- trap_areas / total_area # normalize trap_areas

  if(plot){ #TODO: reconcile this plot with the one from 
            # reweight.marginal.distributions, see 
            # test-functions_sampling-check.R  "MCMC basic synlik"

    if(missing(xlim) && missing(ylim))
      plot(px, py, col = col, type = "l", xlab = xlab, 
           ylab = ylab)
    else if(missing(xlim))
      plot(px, py, col = col, type = "l", xlab = xlab, 
           ylab = ylab, ylim=ylim)
    else if(missing(ylim))
      plot(px, py, col = col, type = "l", xlab = xlab, 
           ylab = ylab, xlim=xlim)
    else
      plot(px, py, col = col, type = "l", xlab = xlab, 
           ylab = ylab, xlim=xlim, ylim=ylim)

    if(plotPoints)
      points(x, y, pch = ".")

  }

  #order the trap areas in reverse order
  rev_trap_areas <- rev(sort(trap_areas))
  rev_trap_index <- rev(order(trap_areas))
  cum_rev_trap_areas <- cumsum(rev_trap_areas)

  whichInterval <- findInterval(cum_rev_trap_areas, prI, 
                                rightmost.closed=TRUE)

  cutoff_ll <- mat.or.vec(length(prI), 1)

  for(i in 0:(length(prI)-1)){
    cutoff_ll[i+1] <- which.min((trap_heights[rev_trap_index])[whichInterval == i])
    cutoff_ll[i+1] <- min(ypairs[rev_trap_index, ][whichInterval==i, ][cutoff_ll[i+1], ])
    if(plot && plotThresholds){
      abline(h=cutoff_ll[i+1], lty = 2)
      text(min(px), cutoff_ll[i+1], label = prI[i+1])
    }
  }

  names(cutoff_ll) <- prI

  crI <- c()
  for(p in cutoff_ll){
      crI <- rbind(crI,range(px[py>p]))
  }
  out <- list(cutoff_ll=cutoff_ll,crI=crI, px=px, py=py, x=x, y=y, norm_fac = total_area)
  return(out)
}

# MultiStatsMcmc
#' @title use MCMC to sample likelihoods according to multiple stats
#' @description Use an MCMC to sample likelihoods of sets of 
#'     data on points
#'     for set of parameters around a TRUE value
#' @details Uses the data to sample the synthetic likelihood for the 
#'     different statistics given at points of higher interest.
#'     At each call, the synthetic likelihood is computed for each 
#'     set of statistics. The acceptation of the proposal is based on
#'     the first stat. 
#'     The results are put in monitor in the form:
#'     paramCurrent,stats1,...,statsn,paramProposed,stats1, ... statsn.
#' @param slObj object, see example
#' @param nbSimul number of iterations in the mcmc 
#' @param nRep number of datasets generated from params
#' @param simulator surcharge to slObj slots
#' @param simulOpt idem
#' @param summaries idem
#' @param statsOpt idem
#' @param params parameter set we are sampling around 
#'     and starting parameter
#' @param correl TODO: add that
#' @param ... additional parameters passed to MCMC
#' @return a matrix with one iteration by line and on each line 
#'      the following structure first for the theta accepted and 
#'      then for theta rejected:
#'     
#'      \code{theta,llDat1Stat1,llDat2Stat2,...,llDatnStatn}
#' @examples
#'  # a simple function generating stochastic data
#'  BasicSimulator<-function(params,simulOpt){
#'    return(rnorm(simulOpt,mean=params[1],sd=params[2]))
#'  }
#' 
#'  # a simple function generating moments stats from stochastic data
#'  BasicStatsMoments<-function(dat,statsOpt){
#'    out<-c(mean(dat),sd(dat))
#'    names(out)<-c("mean","sd") # optional but helps knowing who's who
#'    return(out)
#'  }
#'  # a simple function generating quantile stats from stochastic data
#'  BasicStatsQuant<-function(dat,statsOpt){
#'    return(quantile(dat,probs=statsOpt))
#'  }
#'  # synObj based call
#'  # a synlik object to allow multiple stats comparisons
#'  numberOfDraws<-10
#'  meanTV <- 10
#'  sdTV <- 2
#'  fakeData <- rnorm(numberOfDraws,meanTV,sdTV)
#' basic.synlik.explo<-list(
#'   nObs=50, # repetitions to parameterize the synthetic likelihood
#'   simulator = BasicSimulator, # a normal random generator
#'   summaries = c(BasicStatsMoments,BasicStatsQuant), # test both #							separatly
#'   statsNames = c("Mom","Quant"),
#'   statsOpt = list(NULL,c(0.025,0.5,0.975)),
#'   simulOpt = numberOfDraws, # the number of draws
#'   params = c(meanTV,sdTV),
#'   initValues = c(10,2),
#'   parmNames = c("mean","sd"),
#'   sampling = c("norm","lnorm"),
#'   data = fakeData
#'   )
#' # get the likelihoods, 
#' # Note: nRep should be more 1000 and nbSimul 100 to 1000
#' Monitored<-MultiStatsMcmc(basic.synlik.explo,upFreq=10,nRep=30,nbSimul=30,adapt.sdProp=FALSE)
#' 
#' @export MultiStatsMcmc
MultiStatsMcmc <- function(slObj=NULL,
			   nbSimul=1000,
			   nRep=1000,
			   simulator=slObj$simulator,
			   simulOpt=slObj$simulOpt,
			   summaries=slObj$summaries,
			   statsOpt=slObj$statsOpt,
			   params=slObj$params,
			   correl=TRUE,
			   ...){
  ## define the model
  Model <- function(theta,slObj,...){
    # simulate stats for theta
    dataSimul<-MultipleSimul(n=slObj$nObs,slObj,param=theta)
    statsSimul<-MultipleSummaries(slObj,dataSimul)
    # get stats for the data if needed
    if(is.null(slObj$statsTV)){
      statsData<-MultipleSummaries(slObj,slObj$data)
    }else{
      statsData<-slObj$statsTV
    }
    # compute the likelihoods under theta
    sl<-list()
    for(istat in 1:length(statsData)){ # for each stat
      sl[[istat]]<-synLik(sY=t(statsSimul[[istat]]),sy=t(statsData[[istat]]))
    }

    # transform into a monitor vector
    monitor=c(theta,simplify2array(sl))
    # cat("monitor length:",length(monitor),"\n")
    names(monitor)<-c(slObj$parmNames,
		      rep(slObj$statsNames,each=dim(slObj$statsTV[[1]])[1]))

    # Likelihood to be used
    LP<-sl[[1]][[1]]
    # TODO: add priors

    return(list(LP=LP,monitor=monitor))
  }
  ## define the reference data
  dataTV<-MultipleSimul(n=nRep,slObj,param=params)
  statsTV<-MultipleSummaries(slObj,dataTV)
  slObj$statsTV <- statsTV

  # ## test 
  # out<-Model(c(10,2),slObj)

  Monitored<-MCMC(slObj,Model,monitorBoth=TRUE,useAutoStop=FALSE,
		  nbSimul=nbSimul,...)
  attributes(Monitored)$monitorBoth <- TRUE
  attributes(Monitored)$slObj <- slObj

  return(Monitored)
}

## VisuMultiStatsMcmc
## VisuMultiStatsMcmc <- function(Monitored){
##   slObj<-Monitored$slObj
##   nParams<-length(slObj$params)
##   
##   # check if double Monitored
##   monitorBoth<-attributes(Monitored)$monitorBoth == TRUE
##   if(is.null(monitorBoth)) monitorBoth<-FALSE
## 
##   # put together all parts of Monitored
##   nCol<-dim(Monitored)[2]
##   nColBlock<-nCol/nParams # number of columns per parameter
##   Full<-list()
##   for(i in 1:nParams*2){
##     # bind accepted and rejected
##     firstBegin <- 1+(i-1)*nColBlock
##     firstEnd <- 1+(i-1)*nColBlock + nColBlock/2
## 
##     # Full[[1]] <- rbind(Monitored[,firstBegin:firstEnd]
##     # bind to former
##   }
## 
## }

#' @title Serialize a table in parallel
#' @description Take a table with blocks in parallels, such as 
#'      the output of MCMC/OmniSample with the option monitorBoth 
#'      and returns a table with the blocks one under the other.
#' @param Monitored the table to be serialized
#' @param nParallel the number of blocks on a line
#' @return a table with the blocks one after the other 
#' @examples 
#' mon <- rep(1:4,each=3)
#' mon <- rbind(mon, mon+4)
#' mon2 <- ParallelToSerialMonitor(mon,nParallel=4)
#' @export ParallelToSerialMonitor
ParallelToSerialMonitor <- function(Monitored,nParallel){
  nColPerBlock <- dim(Monitored)[2]/nParallel
  # take care of the names
  Monitored<-as.matrix(Monitored)

  # check if nParallel compatible with Monitored:
  if(nColPerBlock %% 1 >10e-6) stop("n col in Monitored not a multiple of nParallel")
  # serialize
  convertedMonitored <- Monitored[,1:nColPerBlock]
  if(nParallel>1){
    for(i in 2:nParallel){
      b <- (i-1)*nColPerBlock +1
      e <- i*nColPerBlock
      convertedMonitored <- rbind(convertedMonitored,Monitored[,b:e])
    }
  }
  colnames(convertedMonitored) <- colnames(Monitored)[1:nColPerBlock]

  return(convertedMonitored) 
}

# ParallelToSerialMonitor <- function(Monitored, Monitored.df=NULL, nParams=NULL){
#   if(is.null(nParams)){
#     slObj<-Monitored$slObj
#     nParams<-length(slObj$params)
#   }
#   if(is.null(Monitored.df))
#     Monitored.df<-Monitored;
#   
#   modulo<-nParams+2 ##assuming LL, LP stored with each accept/reject
#   nCols <- dim(Monitored.df)[2] 
#   begin <- 1
#   end <- begin+modulo-1
#   convertedMonitored <- Monitored.df[,begin:end]
#   for(index in 2:(nCols/modulo)){
#     begin <- 1+(index-1)*modulo
#     end <- begin+modulo-1
#     tempMonitored <- Monitored.df[,begin:end]
#     names(tempMonitored) <- names(convertedMonitored)
#     convertedMonitored <- rbind(convertedMonitored, tempMonitored)
#   } 
#   return(convertedMonitored) 
# }


# Do NOT remove the NULL hereafter or roxygen is mad
NULL

