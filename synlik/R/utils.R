#========================================
# Tools we don't necessary want to export
#========================================

# count the number of elements with TRUE
# @param vect a vector of TRUE/FALSE, most usefully an expression type vect==0
# @return a scalar of the number of TRUE in the input vector
# @examples 
# thing1<-c(1,2,3,4,5)
# thing2<-rev(thing1)
# count(thing1==thing2)
count<-function(vect){
	return(length(which(vect)))
}

# make multiple simulations, one per column
MultipleSimul <- function(n=100,
			  slObj=NULL,
			  param=slObj$param,
			  simulator=slObj$simulator,
			  simulOpt=slObj$simulOpt){
  out<-t(replicate(n=n,simulator(param,simulOpt)))
}
# there should be as many slObj$summaries as slObj$statsOpt
CheckSummaries <- function(slObj){
  out <- length(slObj$summaries)==length(slObj$statsOpt) | is.null(slObj$statsOpt)
  if(!out) stop("summaries and statsOpt have not same length")
  return(out)
}
# if data is multiple use slObj$summaries on each column
MultipleSummaries <- function(slObj=NULL,
			      data=slObj$data,
			      summaries=slObj$summaries,
			      statsOpt=slObj$statsOpt
			      ){
  if(is.null(dim(data))){
    data <- matrix(data,nrow=1)
  }
  # CheckSummaries(slObj) # check coherence
  if(length(summaries)==1){
    summaries<-list(summaries)
    statsOpt<-list(statsOpt)
  }
  stats<-list()
  for(i_statfn in 1:length(summaries)){
    statfn<-summaries[[i_statfn]]
    stats[[i_statfn]]<-t(apply(data,1,statfn,statsOpt[[i_statfn]]))
  }
  return(stats)
}

# @title resize a matrix/data.frame keeping the values in it
# @description simple routine to resize a matrix or a data.frame keeping the values 
#       of the old matrix in the top left corner and setting everything else to 0
# @param A the matrix or data.frame to resize
# @param nr new number of rows
# @param nc new number of columns
# @return the new matrix or data.frame according to the class of A
Resize<-function(A,nr=nrow(A),nc=ncol(A)){
	B<-as.matrix(mat.or.vec(nr,nc));
	B[1:(dim(A)[1]),1:(dim(A)[2])]<-as.matrix(A)
		
	if(class(A)=="data.frame"){
		B<-as.data.frame(B)
	}
	if(!is.null(colnames(A))){
	  colnames(B)<-colnames(B,do.NULL=FALSE)
	  colnames(B)[1:length(colnames(A))] <- colnames(A)	
	}

	if(!is.null(rownames(A))){
	  rownames(B)<-rownames(B,do.NULL=FALSE)
	  rownames(B)[1:length(rownames(A))] <- rownames(A)	
	}

	return(B);
}


