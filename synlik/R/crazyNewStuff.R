# setwd("H:/corentin/synlik/synlik")
library(testthat)
#' returns the sd of a vector
#' @description returns the sd of a vector using sd()
#' @param values any vector acceptable by sd()
#' @return a real as returned by sd()
#' @examples
#' # need testthat for that
#' library("testthat")
#' # shouldn't return anything
#' expect_equal(sd(c(2,4)),GreatFunction(c(2,4)))
#' # should not be happy
#' expect_equal(sd(c(2,3)),GreatFunction(c(2,4)))
#' @export GreatFunction
GreatFunction <-function(values){
  return(sd(values))
}

expect_equal(sd(c(2,4)),GreatFunction(c(2,4)))

# an other super function
Great2 <- function(values){
  return(paste(values))
}
values<- c("truely","genial","2")
expect_equal(Great2(values),paste(values))

