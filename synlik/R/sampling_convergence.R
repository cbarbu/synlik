#=========================================
# GPL 
# Corentin M. Barbu, Karthik Sethuraman
# Functions useful for diagnostic of MCMC chains 
#=========================================

# TODO : need to switch the evaluation of convergence to the log of the variables 
#        strictly positive (the ones that have a lnorm sampling)
setClass("combinedDiag",
	 representation(colAnalysed = "vector", 
			geweke= "vector",
			limitGeweke="numeric",
			raftery="matrix"),
	 contains="data.frame")

# RafteryLewis
#' @title Raftery-Lewis diagnostic
#' @description Complete diagnostic tool assessing the convergence, 
#'   the burn-in to be discarded, the auto-correlation of the chains
#'   and the necessary number of iterations to get enough samples.
#' @param data matrix of output from MCMC run; rows are the results for each
#'   iteration (assumed to be in a consecutive order), columns
#'   being the different variables.
#' @param varnames vector of names to be tested.  
#'   These names must correspond to
#'   column names in the data matrix.
#' @param q the quantile to be estimated
#' @param r the desired margin of error of the estimate
#' @param s the probability of obtaining an estimate in the interval 
#'   (q-r,q+r)
#' @param epsilon Precision required for estimate of time to convergence
#' @param spacing Frequency at which the MCMC output was recorded; this will
#'   usually be 1.  If only every other MCMC iteration was retained,
#'   then spacing should be set to 2, etc.
#' @param logFile the output can be
#'   redirected from the standard output (if stay default ""), 
#'   to this file, \code{NULL} is no output.
#' @return
#'   A matrix whose rows are the variables tested and whose columns 
#'   contain the following results:
#'   \itemize{
#'     \item{1st col.}{ Kthin, the thinning parameter required to 
#'       make the chain first-order Markov.}
#'     \item{2nd col.}{ = Nburn, the number of iterations needed 
#'       for the burn-in.}
#'     \item{3rd col.}{ = Nprec, the number of iterations required 
#'       to achieve the specified precision.}
#'     \item{4th col.}{ = Nmin, the number of iterations required 
#'       if they were independent.}
#'     \item{5th col.}{ = I_RL, the ratio of Nburn plus Nprec to 
#'       the number of iterations required using independent sampling 
#'       (see Raftery and Lewis, StatSci 1992).}
#'     \item{6th col.}{ = Kind, the thinning parameter required to 
#'       make the chain into an independence chain.}
#'   }
#' @details 
#'  Example values of q, r, s:                                          
#'     0.025, 0.005,  0.95 (for a long-tailed distribution)             
#'     0.025, 0.0125, 0.95 (for a short-tailed distribution);           
#'     0.5, 0.05, 0.95;  0.975, 0.005, 0.95;  etc.                      
#'                                                                      
#'  The result is quite sensitive to r, being proportional to the       
#'  inverse of r^2.                                                     
#'                                                                      
#'  For epsilon, we have always used 0.001.  It seems that the result   
#'  is fairly insensitive to a change of even an order of magnitude in  
#'  epsilon.                                                            
#'  One way to use the program is to run it for several specifications  
#'  of r, s and epsilon and vectors q on the same data set.  When one   
#'  is sure that the distribution is fairly short-tailed, such as when  
#'  q=0.025, then r=0.0125 seems sufficient.  However, if one is not    
#'  prepared to assume this, safety seems to require a smaller value of 
#'  r, such as 0.005.                                                   
#'
#'  This fn is the \code{gibbsit} functions 
#'  as retrieved on http://madison.byu.edu/bayes/gibbsit.txt 
#'  on August 3 2011 and slightly adapted for R by CM Barbu 
#'  changes are between ## CB in the code
#' 
#'  Original comments of the S fn
#'  Version 1.1: August 15, 1995
#' 
#'  An S code translation of the Gibbsit FORTRAN program by Adrian Raftery
#'  and Steven Lewis.  This S function was developed by Steven Lewis from
#'  an earlier function originally written by Karen Vines.
#' 
#'  Developer:  Steven M. Lewis (slewis@@stat.washington.edu).
#'  This software is not formally maintained, but I will be happy to
#'  hear from people who have problems with it, although I cannot
#'  guarantee that all problems will be corrected.
#' 
#'  Permission is hereby granted to StatLib to redistribute this
#'  software.  The software can be freely used for non-commercial
#'  purposes, and can be freely distributed for non-commercial
#'  purposes only.  The copyright is retained by the developer.
#' 
#'  Copyright 1995  Steven M. Lewis, Adrian E. Raftery and Karen Vines
#' 
#' 
#'  References:
#' 
#'  Raftery, A.E. and Lewis, S.M. (1992).  How many iterations in the
#'  Gibbs sampler?  In Bayesian Statistics, Vol. 4 (J.M. Bernardo, J.O.
#'  Berger, A.P. Dawid and A.F.M. Smith, eds.). Oxford, U.K.: Oxford
#'  University Press, 763-773.
#'  This paper is available via the World Wide Web by linking to URL
#'    http://www.stat.washington.edu/tech.reports and then selecting
#'  the "How Many Iterations in the Gibbs Sampler" link.
#'  This paper is also available via regular ftp using the following
#'  commands:
#'    ftp ftp.stat.washington.edu (or 128.95.17.34)
#'    login as anonymous
#'    enter your email address as your password
#'    ftp> cd /pub/tech.reports
#'    ftp> get raftery-lewis.ps
#'    ftp> quit
#' 
#'  Raftery, A.E. and Lewis, S.M. (1992).  One long run with diagnos-
#'  tics: Implementation strategies for Markov chain Monte Carlo.
#'  Statistical Science, Vol. 7, 493-497.
#' 
#'  Raftery, A.E. and Lewis, S.M. (1995).  The number of iterations,
#'  convergence diagnostics and generic Metropolis algorithms.  In
#'  Practical Markov Chain Monte Carlo (W.R. Gilks, D.J. Spiegelhalter
#'  and S. Richardson, eds.). London, U.K.: Chapman and Hall.
#'  This paper is available via the World Wide Web by linking to URL
#'    http://www.stat.washington.edu/tech.reports and then selecting
#'  the "The Number of Iterations, Convergence Diagnostics and Generic
#'  Metropolis Algorithms" link.
#'  This paper is also available via regular ftp using the following
#'  commands:
#'    ftp ftp.stat.washington.edu (or 128.95.17.34)
#'    login as anonymous
#'    enter your email address as your password
#'    ftp> cd /pub/tech.reports
#'    ftp> get raftery-lewis2.ps
#'    ftp> quit
#' @seealso CombinedDiag
#' @export RafteryLewis
RafteryLewis <- function(data, varnames = NULL, q = 1/40, r =
	  1/80, s = 19/20, epsilon = 1/1000, spacing = 1, logFile = ""){
        # compute the necessary number of independant samples
	phi <- qnorm((s + 1)/2)
	nmin <- as.integer(ceiling((q * (1 - q) * phi^2)/r^2))
	# shorten the chain if needed
## CB TODO: figure out if possible to do a thinning and not just cut
	if(length(data[,1])>45000){
	  warning("\nR limitations on integer manipulations doesn't allow to handle all this matrix\n Analysis limited to the last 45000 iterations\n")
	  data<-data[-(1:(length(data[,1])-45000)),]
	}	
	if(is.null(varnames)){
		data<-as.data.frame(data)
		varnames=dimnames(data)[[2]]
		if(is.null(varnames)){
			varnames<- as.character(seq(1:dim(data)[2]))
		}
	}
## CB
	resmatrix <- matrix(NA, nrow = length(varnames), ncol = 6)
	iteracnt <- nrow(data)
	if(!is.null(logFile))
		cat("Results when q = ", q, ", r = ", r, ", s = ", s,
			", epsilon = ", epsilon, ":\n\n", 
			file = logFile, sep = "",append=TRUE)
	for(r1 in 1:length(varnames)) {
		quant <- quantile(data[, (curname <- varnames[r1])], probs = q)
		dichot <- as.integer(data[, curname] <= quant)	#
#	First find the actual thinning parameter, kthin.
		testtran <- array(as.integer(0), dim = c(2, 2, 2))
		kwork <- 0
		bic <- 1
		while(bic >= 0) {
			kwork <- as.integer(kwork + 1)
			testres <- dichot[seq(1, iteracnt, by = kwork)]
			thindim <- length(testres)
			tttemp <- as.integer(testres[1:(thindim - 2)] + testres[
				2:(thindim - 1)] * 2 + testres[3:thindim] * 4)
			testtran[1, 1, 1] <- sum(as.integer(tttemp == 0))
			testtran[2, 1, 1] <- sum(as.integer(tttemp == 1))
			testtran[1, 2, 1] <- sum(as.integer(tttemp == 2))
			testtran[2, 2, 1] <- sum(as.integer(tttemp == 3))
			testtran[1, 1, 2] <- sum(as.integer(tttemp == 4))
			testtran[2, 1, 2] <- sum(as.integer(tttemp == 5))
			testtran[1, 2, 2] <- sum(as.integer(tttemp == 6))
			testtran[2, 2, 2] <- sum(as.integer(tttemp == 7))
			g2 <- 0
			for(i1 in 1:2) {
				for(i2 in 1:2) {
				  for(i3 in 1:2) {
				    if(testtran[i1, i2, i3] != 0) {
				      fitted <- (sum(testtran[i1, i2, 1:2]) *
				        sum(testtran[1:2, i2, i3]))/(sum(
				        testtran[1:2, i2, 1:2]))
				      g2 <- g2 + testtran[i1, i2, i3] * log(
				        testtran[i1, i2, i3]/fitted) * 2
				    }
				  }
				}
			}
			bic <- g2 - log(thindim - 2) * 2
		}
		kthin <- as.integer(kwork * spacing)	#
#	Now determine what the thinning parameter needs to be to achieve
#	independence, kmind.
		indptran <- matrix(as.integer(0), nrow = 2, ncol = 2)
		firsttime <- TRUE
		bic <- 1
		while(bic >= 0) {
			if(!firsttime) {
				kwork <- as.integer(kwork + 1)
				testres <- dichot[seq(1, iteracnt, by = kwork)]
				thindim <- length(testres)
			}
			indptemp <- as.integer(testres[1:(thindim - 1)] +
				testres[2:thindim] * 2)
			indptran[1, 1] <- sum(as.integer(indptemp == 0))
			indptran[2, 1] <- sum(as.integer(indptemp == 1))
			indptran[1, 2] <- sum(as.integer(indptemp == 2))
			indptran[2, 2] <- sum(as.integer(indptemp == 3))
			if(firsttime) {
#	Save this particular transfer matrix for later use in calculating the
#	length of the burn-in and for the required precision.
				finaltran <- indptran
				firsttime <- FALSE
			}
			den <- rep(apply(indptran, 1, sum), 2) * rep(apply(
				indptran, 2, sum), c(2, 2))
			g2 <- sum(log((indptran * (dcm1 <- thindim - 1))/den) *
				indptran, na.rm = TRUE) * 2
			bic <- g2 - log(dcm1)
		}
		kmind <- as.integer(kwork * spacing)	#
#	Next find the length of burn-in and the required precision.
		alpha <- finaltran[1, 2]/(finaltran[1, 1] + finaltran[1, 2])
		beta <- finaltran[2, 1]/(finaltran[2, 1] + finaltran[2, 2])
		tempburn <- log((epsilon * (alpha + beta))/max(alpha, beta))/(
			log(abs(1 - alpha - beta)))
		nburn <- as.integer(ceiling(tempburn) * kthin)
		tempprec <- ((2 - alpha - beta) * alpha * beta * phi^2)/(((
			alpha + beta)^3) * r^2)
		nprec <- as.integer(ceiling(tempprec) * kthin)
		iratio <- (nburn + nprec)/nmin
		kind <- max(floor(iratio + 1), kmind)
		resmatrix[r1,  ] <- c(kthin, nburn, nprec, nmin, round(iratio,
			digits = 2), kind)
		  if(!is.null(logFile))
			cat("  ", curname, "\tKthin = ", kthin, "\tNburn = ",
				nburn, "\tNprec = ", nprec, "\tNmin = ", nmin,
				"\tI_RL = ", resmatrix[r1, 5], "\tKind = ",
				kind, "\n", file = logFile, sep = "", append =
				TRUE)
	}
	dimnames(resmatrix) <- list(varnames, c("Kthin", "Nburn", "Nprec",
		"Nmin", "I_RL", "Kind"))
	return(resmatrix)
}

# GewekeDiagnostic
#' @title Geweke diagnostic
#' @description apply the Geweke diagnostic to a chain. 
#'   Returns the z-score and associated p-value for probability of
#'   difference between the beginning and the end of the chain.
#'   Initially 
#'   copied from LaplacesDemon, not anymore on CRAN, and slightly adapted.
#' @param db a matrix or data.frame with different variables in columns
#'   and iterations in lines
#' @param frac1 pourcentage of the first fraction of the chain
#' @param frac2 pourcentage of the second fraction of the chain
#' @details This diagnostic is based on the spectral estimation of the 
#'   standard deviation in the chain. 
#'   For the original package check 
#'   http://www.bayesian-inference.com/software
#'   See original article: Geweke1992
#' @return a list with:
#'   \itemize{
#'     \item{\code{z}}{ the z-score for the diagnostic}
#'     \item{\code{pVal}}{ p-value corresponding to the z-score}
#'   }
#' @seealso CombinedDiag
#' @export GewekeDiagnostic
GewekeDiagnostic <- function (db,frac1=0.1,frac2=0.5) {
  x <- as.matrix(db)
  startx <- 1
  endx <- nrow(x)
  xstart <- c(startx, endx - frac2 * {endx - startx})
  xend <- c(startx + frac1 * {endx - startx}, endx)

  y.variance <- y.mean <- vector("list", 2)
  for (i in 1:2) {
    y <- x[xstart[i]:xend[i], ]
    y.mean[[i]] <- colMeans(as.matrix(y))
    yy <- as.matrix(y)
    y <- as.matrix(y)
    max.freq <- 0.5
    order <- 1
    max.length <- 200
    if (nrow(yy) > max.length) {
      batch.size <- ceiling(nrow(yy)/max.length)
      yy <- aggregate(ts(yy, frequency = batch.size), nfreq = 1, 
		      FUN = mean)
    }
    else {
      batch.size <- 1
    }
    yy <- as.matrix(yy)
    fmla <- switch(order + 1, spec ~ one, spec ~ f1, spec ~ 
		   f1 + f2)
    if (is.null(fmla)) 
      stop("Invalid order.")
    N <- nrow(yy)
    Nfreq <- floor(N/2)
    freq <- seq(from = 1/N, by = 1/N, length = Nfreq)
    f1 <- sqrt(3) * {
      4 * freq - 1
    }
    f2 <- sqrt(5) * {
      24 * freq * freq - 12 * freq + 1
    }
    v0 <- numeric(ncol(yy))
    for (j in 1:ncol(yy)) {
      # cat("Gew:",names(db)[j],"\n")
      zz <- yy[, j]
      zz<-zz[which(is.finite(zz))]
      if (var(zz) == 0) {
	v0[j] <- 0
      }
      else {
	yfft <- fft(zz)
	spec <- Re(yfft * Conj(yfft))/N
	spec.data <- data.frame(one = rep(1, Nfreq), 
				f1 = f1, f2 = f2, spec = spec[1 + {
				  1:Nfreq
				}], inset = I(freq <= max.freq))
		glm.out <- try(glm(fmla, family = Gamma(link = "log"), 
				   data = spec.data), silent = TRUE)
	if (!inherits(glm.out, "try-error")) 
	  v0[j] <- predict(glm.out, type = "response", 
			   newdata = data.frame(spec = 0, one = 1, f1 = -sqrt(3), 
						f2 = sqrt(5)))
      }
    }
    spec <- list(spec = v0)
    spec$spec <- spec$spec * batch.size
    y.variance[[i]] <- spec$spec/nrow(y)
  }
  z <- {
    y.mean[[1]] - y.mean[[2]]
  }/sqrt(y.variance[[1]] + y.variance[[2]])
  # print(cbind(y.mean[[1]],y.mean[[2]],y.variance[[1]],y.variance[[2]]))
  if (any(y.variance[[1]]==0) || any(y.variance[[2]]==0)){
	  stop("Geweke spectral variance null")
  }
  pvalue<-pnorm(-abs(z))

  return(list(z=z,pVal=pvalue))
}

# stubb not using the spectral decomposition to estimate
# the standard deviation
basicGeweke<-function(db,frac1=0.1,frac2=0.5){
  x<-as.matrix(db)
  startx <- 1
  endx <- nrow(x)
  xstart <- c(startx, endx - frac2 * {endx - startx})
  xend <- c(startx + frac1 * {endx - startx}, endx)
  ymean<-yvar<-mat.or.vec(dim(db)[2],2)
  for(samp in 1:2){
    for(fact in 1:dim(db)[2]){
      y<-db[xstart[samp]:xend[samp],fact]
      ymean[fact,samp]<-mean(y)
      yvar[fact,samp]<-var(y)
    }
  }
  zscore<-(ymean[,1]-ymean[,2])/sqrt(yvar[,1]+yvar[,2])
  pvalue<-pnorm(-abs(zscore))
  return(list(z=zscore,pVal=pvalue))
}
###########################################################################
# ESS or Effective Size
# Taken from: LaplacesDemon v10.12.30 
#
# The purpose of this function is to estimate the effective sample size   
# of a target distribution after taking autocorrelation into account.    
# Although the code is slightly different, it is essentially the same as 
# the effectiveSize function in the coda package.
# This function is called inside CombinedDiag and thus needs to be here
###########################################################################
ESS <- function(x){
  x <- as.matrix(x)
  v0 <- order <- numeric(ncol(x))
  names(v0) <- names(order) <- colnames(x)
  z <- 1:nrow(x)
  for (i in 1:ncol(x))
  {
    lm.out <- lm(x[, i] ~ z)
    if (identical(all.equal(sd(residuals(lm.out)), 0), TRUE)) {
      v0[i] <- 0
      order[i] <- 0
    }
    else {
      ar.out <- ar(x[, i], aic = TRUE)
      v0[i] <- ar.out$var.pred/(1 - sum(ar.out$ar))^2
      order[i] <- ar.out$order
    }
  }
  spec <- list(spec = v0, order = order)
  spec <- spec$spec
  Eff.Size <- ifelse(spec == 0, 0, nrow(x) * apply(x, 2, var)/spec)
  Eff.Size <- ifelse(Eff.Size > nrow(x), nrow(x), Eff.Size)
  return(Eff.Size)
}




# CombinedDiag
#' @title combined diagnostic on one chain 
#' @description Combines the Raftery-Lewis and Geweke diag
#'   to estimate if a chain has been sampled enough
#' @param sampBrut a matrix or data.frame with in columns the 
#'   different variables surveyed and on each line a different interation
#' @param baseLimitGeweke p-value threshold for one parameter 
#' @param KthinInit keep only 1 in \code{KthinInit} values for the analysis.
#'   Useful to speed up the diagnostic on extremely long chains.
#' @param logFile This functions is quite verbose (default =""), 
#    the output can be
#'   redirected to this logFile, NULL is no output.
#' @return an object of class \code{combinedDiag} with following parts:
#'   \itemize{
#'     \item{$ok}{ logical. Is the sampling enough?}
#'     \item{$newNbIt}{ Integer. What is the estimated length of the 
#'       chain needed to get enough samples? 
#'       Equal to \code{dim(sampBrut)[1]} if enough samples.}
#'     \item{$nbItEff}{ Integer. Number of "efficient" iterations, 
#'       according to the Raftery-Lewis}
#'     \item{$burnIn}{ Integer. Length of the burn-in.}
#'     \item{$Kthin}{ Integer. Initially applied thinning.}
#'     \item{$Kind}{ Integer. Recommended thinning to prevent 
#'       auto-correlation}
#'     \item{$minGewPvalue}{ Integer. The minimum p-value obtained 
#'       for the Geweke on each of the monitored variables.}
#'     \item{@@geweke}{ Vector of p-values for the Geweke on each monitored
#'       variable}
#'     \item{@@limitGeweke}{ threshold applied on each p-value in the 
#'       Geweke (baseLimitGeweke corrected for multiple testing 
#'       by the Bonferroni method).}
#'     \item{@@raftery}{ Full output of the RafteryLewis function}
#'   }
#' 
#' @details Use the Raftery-Lewis test first, when this test is passed
#'   (convergence and enough iterations given the correlation of the chain),
#'   it applies the Geweke to the chain, after the burn-in as identified
#'   by the Raftery-Lewis. If both are passed, the sampling is considered
#'   long enough.
#' @examples
#' fakeSamples<-matrix(rnorm(10000),ncol=10)
#' diag<-CombinedDiag(fakeSamples)
#' # diag$ok should be 1
#' @seealso GewekeDiagnostic,RafteryLewis,MCMC
#' @export CombinedDiag
CombinedDiag<-function(sampBrut,baseLimitGeweke=0.05,KthinInit=1,logFile=""){
  enoughIt<-FALSE;
  nbItEff<-0
  resG<-0
  discard<-0
  limitGeweke<-0
  nbItBrut<-length(sampBrut[,1])
  sampThined<-sampBrut[seq(1,nbItBrut,KthinInit),];
  # eliminate constant parameters

  colAnalysed<-which(apply(sampBrut,2,var)!=0)
  if(length(colAnalysed)<1){
    stop("CombinedDiag:all columns of sampBrut are constant\n")
  }else{
    sampThined<-sampThined[,colAnalysed]
  }

  nbIt<-length(sampThined[,1])
  # cat("sampThined:\n");
  # print(str(sampThined))

  ## find the minimum number of iterations according to the raftery
  resRafLeft<-RafteryLewis(sampThined,logFile=logFile)
  # print(resRafLeft)
  ## much too difficult to reach:
  # resRafMean<-RafteryLewis(sampThined,q=20/40) 
  # print(resRafMean)
  resRafRight<-RafteryLewis(sampThined,q=39/40,
			   logFile=logFile)
  # print(resRafRight)
  resRaf<-rbind(resRafLeft,resRafRight)
  resRafMax<-apply(resRaf,2,max,na.rm=TRUE)
  nbItMin<-as.integer(resRafMax[2]+resRafMax[3])*KthinInit;
  Kthin<-resRafMax[1]*KthinInit;
  Kind<-resRafMax[6]*KthinInit;
  if(!is.null(logFile)){
    cat("new Kthin:",Kthin,"Kind",Kind,"\n",file=logFile,append=TRUE);
  }
  burnIn<-resRafMax[2]*KthinInit;
  Nmin<-resRafMax[4];
  if(nbItMin<=nbItBrut){
    if(!is.null(logFile))
      cat("Raftery positive (",
	  nbItBrut,">=",nbItMin,")\n",file=logFile,append=TRUE);
    ## if enough iterations according to the raftery, 
    ## test if Geweke ok on non burnin 
    discard<-burnIn;
    result<-TRUE;
    nbRep<-1
    nMaxRep<-5
    # account for the multiple variables 
    # that need to pass the test
    limitGeweke<-1-(1-baseLimitGeweke)^(nMaxRep/dim(sampThined)[2])
    while( nbRep<=nMaxRep && min(resG)<limitGeweke && class(result)!="try-error"){
      resGbrut<-1+limitGeweke # something bigger than limitGeweke
      discard<-burnIn+round((nbIt-burnIn)*(nbRep-1)*0.1);
      gewekeSample<-sampThined[-(1:discard),]
      if(!is.null(logFile))
	cat("test Geweke with",discard,"discarded and limit=",
	    limitGeweke,"\n",file=logFile,append=TRUE);
      result<-try(resGbrut<-GewekeDiagnostic(gewekeSample,frac1=0.5),silent=FALSE) # return p-values, 
      resG<-resGbrut$pVal
      # print(resG)

      nbRep<-nbRep+1
    }
    if(min(resG)<limitGeweke){
      ## if the convergence is not ok according to Geweke
      ## increase the number of iterations substentially
      if(!is.null(logFile)){
	cat("Geweke not ok:",resG,"(at least one <",limitGeweke,")\n",file=logFile,append=TRUE);
      }
      newNbIt<-ceiling(nbItBrut*1.5);
    }else{
      ## if the convergence is ok test if we have enough samples
      if(!is.null(logFile)){
	cat("Geweke ok :",resG,"(all >",limitGeweke,"), burnIn=",discard*KthinInit,"\n",file=logFile,append=TRUE);
      }
      resESS<-ESS(sampThined[-(1:discard),])
      nbItEff<-ceiling(max(resESS))
      if(nbItEff<Nmin){
	if(!is.null(logFile)){
	  cat("Need some more iterations, only",nbItEff,"efficient iterations, we need:",Nmin,")\n",file=logFile,append=TRUE);
	}
	## not yet enough iterations to get the precision we need
	newNbIt<-as.integer(ceiling(Nmin*nbItBrut/nbItEff));
      }else{
	if(!is.null(logFile)){
	  cat("All test passed, enough iterations (",nbItEff," it. eff. >",Nmin,")\n",file=logFile,append=TRUE);
	}
	enoughIt<-TRUE
	newNbIt<-nbItBrut
      }
    }
  }else{
    if(!is.null(logFile)){
      cat("Raftery negative (",nbItBrut,"<",nbItMin,")\n",file=logFile,append=TRUE);
    }
    newNbIt<-nbItMin;
  }
  output<-as.data.frame(t(c(enoughIt,newNbIt,nbItEff,KthinInit*max(discard,burnIn),Kthin,Kind,min(resG))))
  names(output)<-c("ok","newNbIt","nbItEff","burnIn","Kthin","Kind","minGewPvalue")

  return(invisible(new("combinedDiag",output,colAnalysed=colAnalysed,geweke=resG,limitGeweke=limitGeweke,raftery=resRaf)));
}

# GelmanRubinWrapper
#' Gelman-Rubin test
#' @description Perfom a Gelman-Rubin test. Require the \code{boa} 
#'   package as it is actually a wrapper for \code{boa.chain.gandr} 
#'   which is a bit picky.
#' @param listChains a list of mcmc results to be compared. Each mcmc
#'   result should be a table with in columns the variables, 
#'   rows corresponding to the iterations of the mcmc
#' @param mins a vector with the theoretical minimums for each variable
#' @param maxs a vector with the theoretical maximums for each variable
#' @param alpha quantile (1-alpha/2) at which to estimate the upper 
#'   limit of the shrink factor
#' @param tr Threshold for the Gelman-Rubin statistic 
#'   (default 1.05, 1.1 enough according to Gelman2004, p. 297)
#' @return output of \code{boa.chain.gandr} plus:
#'   \itemize{
#'     \item{\code{$ok}}{ logical: Gelman Rubin ok}
#'     \item{\code{$tr}}{ threshold \code{tr} used to see if check ok.}
#'   }
#' @examples
#'   set.seed(1000) # just to avoid very unlikely bad sampling
#'   # fake chains
#'   chain1 <- cbind(rnorm(10000),rlnorm(10000,10,1))
#'   chain2 <- cbind(rnorm(10000),rlnorm(10000,10,1))
#'   # prepare for the analysis
#'   mins <- c(-Inf,0);
#'   maxs <- c(+Inf,+Inf);
#'   chains<-list(chain1,chain2)
#'   chainsBis<-list(chain1,chain2)
#'   # analysis
#'   gr<-GelmanRubinWrapper(chains,mins,maxs)
#'   # gr$ok should be TRUE
#' @seealso boa.chain.gandr
#' @export GelmanRubinWrapper

GelmanRubinWrapper<-function(listChains,mins,maxs,alpha=0.05,tr=1.05){
  require(boa)
  # be sure format will not be an issue
  nChains<-length(listChains);
  for(chainId in 1:nChains){
    listChains[[chainId]] <- as.data.frame(listChains[[chainId]])
  }
  names(listChains)<-as.character(seq(1:nChains));

  # make the chains.support argument
  support <- rbind(mins,maxs);
  colnames(support) <- colnames(listChains[[1]]);
  chains1.support <- list();
  for(i in 1:nChains){
    chains1.support[[i]] <- support;
  }
  names(chains1.support)<-names(listChains);

  # make analysis
  gr1 <- boa.chain.gandr(listChains, chains1.support,  alpha = 0.05);

  # summarize output
  gr1[["ok"]]<- all(gr1[["csrf"]][,2]<tr);
  gr1[["tr"]]<-tr;

  return(gr1);
}

